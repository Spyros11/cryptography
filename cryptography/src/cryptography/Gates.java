package cryptography;

public class Gates {

	public static final int TRUE = 1;
	public static final int FALSE = 0;
	public static Encryption e = new Encryption();

	public static String keyA0 = e.randomGen(32);
	public static String keyA1 = e.randomGen(32);
	public static Double test = Double.parseDouble(e.randomGen(32));
	

	public static String keyB0 = e.randomGen(32);
	public static String keyB1 = e.randomGen(32);

	public static String keyX0 = e.randomGen(32);
	public static String keyX1 = e.randomGen(32);

	public static String gateID = e.randomGen(3);
	public static String keyA, keyB, keyX;

	public static char f ;
	public static int last;
	public static int prelast;
	
	/* ends with
	 * 1,1 cipher -> 0,0 
	 * 1,0 cipher -> 0,1 
	 * 0,1 cipher -> 1,0 
	 * 0,0 cipher -> 1,1
	 */
	public static int [] notGate(int key) {
		// end bit
		int result;

		if (key < 0 || key > 1) {
			System.out.println("wrong entry");
			System.exit(1);
		}

		if (key == 1) {
			keyA = keyA1;
			prelast = 0;
			result = 0;
			keyX = keyX0;
		} else {
			keyA = keyA0;
			prelast = 1;
			
			keyX = keyX1;
			result = 1;			
		}

		String hexadecimal = e.encryptText(e.hash(keyA, "", gateID));
		
		String binary = e.hexadecimalToBinary(hexadecimal);
		
		// kx0 bits are less needs padding
		//System.out.println("kX\n" + keyX);

		char[] cha = e.store(binary);
		// to be changed its not always KX0!!!!
		char[] chb = e.store(keyX);

		// prints an int array
		int array[] = e.xorArrays(cha, chb);

		//System.out.println("\nXOR array");

		//e.showXorArray(array);
		
		array[array.length-1] = prelast;

		
		return array;
	}

	public static int[] andGate(int key1, int key2) {
		int result;

		if (key1 < 0 || key1 > 1 || key2 < 0 || key2 > 1) {
			System.out.println("wrong entry");
			System.exit(1);
		}
		if (key1 == 0) {
			keyA = keyA0;
			prelast = 1;
			result = 0;
		} else {
			keyA = keyA1;
			prelast = 0;
			result = 1;
		}
		if (key2 == 0) {
			keyB = keyB0;
			last = 1;
			result = 0;
		} else {
			keyB = keyB1;
			last = 0;
			result = 0;
		}
		if(key1 == 1 && key2 == 1) {
			keyX = keyX1;
			result = 1;
		} else {
			keyX = keyX0;
			result = 0;
		
		}
			

		String hexadecimal = e.encryptText(e.hash(keyA, keyB, gateID));
		String binary = e.hexadecimalToBinary(hexadecimal);
		//System.out.println("binary\n" + binary);
		
		// kx0 bits are less needs padding
		//System.out.println("kX\n" + keyX);

		char[] cha = e.store(binary);
		// to be changed its not always KX0!!!!
		char[] chb = e.store(keyX);

		// prints an int array
		int array[] = e.xorArrays(cha, chb);

		//e.showXorArray(array);
		
		array[array.length-2] = last;
		array[array.length-1] = prelast;

		return array;
	}

	public static int[] orGate(int key1, int key2) {
		int result;

		if (key1 < 0 || key1 > 1 || key2 < 0 || key2 > 1) {
			System.out.println("wrong entry");
			System.exit(1);
		}

		if (key1 == 0) {
			keyA = keyA0;
			prelast = 1;
			
			if(key2 == 0) {
				keyB = keyB0;
				result = 0;
				keyX = keyX0;
				last = 1;
			} else {
				keyB = keyB1;
				result = 1;
				keyX = keyX1;
				last = 0;
			}
		} else {
			keyA = keyA1;
			prelast = 0;

			if(key2 == 0) {
				keyB = keyB0;
				last = 1;
				result = 1;
				keyX = keyX1;
			} else {
				keyB = keyB1;
				last = 0;
				result = 1;
				keyX = keyX1;
			}
		}

		String hexadecimal = e.encryptText(e.hash(keyA, keyB, gateID));
		String binary = e.hexadecimalToBinary(hexadecimal);
		//System.out.println("binary\n" + binary);
		
		// kx0 bits are less needs padding
		//System.out.println("kX\n" + keyX);

		char[] cha = e.store(binary);
		// to be changed its not always KX0!!!!
		char[] chb = e.store(keyX);

		// prints an int array
		int array[] = e.xorArrays(cha, chb);


		array[array.length-2] = last;
		array[array.length-1] = prelast;

		//e.showXorArray(array);


		return array;
	}

	public static int[] xorGate(int key1, int key2) {
		int result = 0;

		if (key1 < 0 || key1 > 1 || key2 < 0 || key2 > 1) {
			System.out.println("wrong entry");
			System.exit(1);
		}

		if(key1 == 0) {
			keyA = keyA0;	
			prelast = 1;

		} else {
			keyA = keyA1;	
			prelast = 0;

		}

		if(key2 == 0) {
			keyB = keyB0;
			last = 1;
		} else {
			keyB = keyB1;
			last = 0;
		}
		
		if(keyA != keyB) {
			result = 1;
			keyX = keyX1;
		} else {
			result = 0;
			keyX = keyX0;
		
		}
			
		String hexadecimal = e.encryptText(e.hash(keyA, keyB, gateID));
		String binary = e.hexadecimalToBinary(hexadecimal);
		//System.out.println("binary\n" + binary);
		
		// kx0 bits are less needs padding
		//System.out.println("kX\n" + keyX);

		char[] cha = e.store(binary);
		// to be changed its not always KX0!!!!
		char[] chb = e.store(keyX);

		// prints an int array
		int array[] = e.xorArrays(cha, chb);

		array[array.length-2] = last;
		array[array.length-1] = prelast;
		
		e.showXorArray(array);
		
		return array;
	}
}
