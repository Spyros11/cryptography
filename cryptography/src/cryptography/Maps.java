package cryptography;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Maps {

	static Gates g1 = new Gates();
	
	public static void notTable() {
		HashMap<Integer, String> notGateC = new HashMap();
		HashMap<Integer, int[]> notGateB = new HashMap();
		
		for(int key=0; key<2; key++) {
			
			notGateC.put(key, "Ek"+key +"A(K"+g1.notGate(key) +"B)\t");
			notGateB.put(key, g1.notGate(key));

		}
		
		System.out.println("Cipher for NOT gate\n" +notGateC +"\nBIT\n" +notGateB);
		
		
	}
	
	public static void andTable() {
		HashMap<Integer, String> andGateC = new HashMap();
		HashMap<Integer, int[]> andGateB = new HashMap();
		int i=0;
		
		for(int key1=0; key1<2; key1++) {
			for(int key2=0; key2<2; key2++) {
				//value.add(key2+key1, g1.andGate(key1, key2));	// to key +key me xalaei
				andGateB.put(i, g1.andGate(key1, key2));
				andGateC.put(i, "Ek"+key2 +"A(K" +key1 +"B(K" +g1.andGate(key1,key2) +"x))\t");
				
				System.out.println("key1 = " +key1 +" key2 = " +key2 +" ---> " +andGateB.get(i));
				i++;
			}
		}
		System.out.println(andGateC);
	}
	
	public static void orTable() { 
		HashMap<Integer, String> orGateC = new HashMap();
		HashMap<Integer, int[]> orGateB = new HashMap();
		int i=0;
		
		for(int key1=0; key1<2; key1++) {
			for(int key2=0; key2<2; key2++) {
				//value.add(key2+key1, g1.andGate(key1, key2));	// to key +key me xalaei
				//orGateB.put(i, g1.andGate(key1, key2));
				orGateC.put(i, "Ek"+key2 +"A(K" +key1 +"B(K" +g1.andGate(key1,key2) +"x))\t");
				
				System.out.println("key1 = " +key1 +" key2 = " +key2 +" ---> " +orGateB.get(i));
				i++;
			}
		}
		System.out.println(orGateC);
	}

	
}
