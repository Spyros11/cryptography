package cryptography;

public class FullAdder {

	/* Input	output
	 * A B Cin | Cout S
	 * 0 0 0   | 0	  0
	 * 0 1 0   | 0    1
	 * 1 0 0   | 0    1
	 * 1 1 0   | 1    0
	 * 0 0 1   | 0    1
	 * 0 1 1   | 1    0
	 * 1 0 1   | 1    0
	 * 1 1 1   | 1    1
	 */
	
	public static Encryption e = new Encryption();
	public static Gates gate = new Gates();

	public static void method(int a, int b) {
		// first output
		int [] array = gate.xorGate(a, b);
		System.out.println(array[array.length-2] + array[array.length-1]);
		
		// second
		
		//gate.orGate(array, a);
	}
	
}
