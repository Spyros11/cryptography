package cryptography;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class Encryption {

	static Random random = new Random();
	
	public static String encryptText(String text) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");

			// Change this to UTF-16 if needed
			md.update(text.getBytes(StandardCharsets.UTF_8));
			byte[] digest = md.digest();

			String hex = String.format("%064x", new BigInteger(1, digest));
			//System.out.println(hex);
			return hex;

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String randomGen(int y) {
		String key[] = new String[35];
		String word = "";
		int number;

		for (int i = 1; i <= y; i++) {
			number = random.nextInt(2);
			key[i] = String.valueOf(number);
			word = word + key[i];
		}
		return word;
	}

	public static String hash(String kA, String kB, String gID) {
		String word = new String();
		word = kA + kB + gID;

		return word;

	}
	
	
	public static String hexadecimalToBinary(String s) {
		return new BigInteger(s, 16).toString(2);
	}

	public static char[] xorStrings(String a, String b) {
		char[] chars = new char[a.length()];
		char[] charsb = new char[b.length()];
		char[] charsc = new char[500];
		int v = 0;
		for (int i = 0; i < chars.length; i++) {
			charsc[v] = (char) (chars[i] ^ charsb[i]);
			v++;

		}
		return chars;
	}

	public static char[] store(String a) {

		// Creating array of string length
		char[] ch = new char[a.length()];

		// Copy character by character into array
		for (int i = 0; i < a.length(); i++) {
			ch[i] = a.charAt(i);
		}

		return ch;
	}

	public static int[] xorArrays(char[] a, char[] b) {
		int[] d = new int[a.length];
		
		char [] c = padding(a, b);
		
		//System.out.println();
		for (int i = 0; i < a.length; i++) {

			d[i] = a[i] ^ c[i];
		}
		
		return d;
	}

	public static void showXorArray(int[] array) {
		for (int i = 0; i < array.length-1; i++) {
			System.out.print(array[i]);
		}
	}

	public static int randomKey(int [] array) {

		int spot = random.nextInt(10);
		
		return spot;
	}
	public static char[] padding(char[] a, char[] b) {
		char[] c = new char[a.length];

		if (a.length != b.length) {
			//System.out.println("Different lengths\n");

			for (int j = 0; j < b.length; j++) {
				c[j] = b[j];
			}
			for (int i = b.length; i < a.length; i++) {
				c[i] = '0';
			}
				
		}
		return c;
	}

}
